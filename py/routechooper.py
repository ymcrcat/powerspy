#!/usr/bin/env python
from geopy.geocoders import GoogleV3
from geopy.distance import vincenty
from pprint import pprint
import os
import sys
from collections import namedtuple
import pandas

class bcolors:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'

# Define a tuple type for location coordinates
Location = namedtuple("Location", "latitude longitude")

THRESHOLD = 2000 # in meters

def find_location(geolocator, intersection):
	while True:
		try:
			iloc = geolocator.geocode(intersection, timeout=5)
		except ssl.SSLError:
			print "Got an error connecting to Google Maps. Try again."
			continue
		break
	return iloc

def get_location_from_tuple(intersection):
        lat_str, lon_str = intersection.split(",")
        return Location(float(lat_str), float(lon_str))

def get_intersection_locations(intersections, geolocator):
    iloc_list = []

    for i in intersections:
        try:
            iloc = get_location_from_tuple(i)
        except:
            iloc = find_location(geolocator, i)
            iloc = Location(iloc[1][0], iloc[1][1])

        iloc_list.append(iloc)

    return iloc_list

def main():
    if len(sys.argv) < 3:
        sys.exit("Usage: cut_segements.py <route file> <intersection file> <output dir>")

    log_filename = sys.argv[1]
    intersections_filename = sys.argv[2]
    outdir = sys.argv[3]

    geolocator = GoogleV3()

    # a dictionary the maps a point on the route (indexed by it line if the routefile) to an intersection
    intersection_indices  = []

    if not os.path.exists(outdir):
        os.makedirs(outdir)
    # else: # if directory already exists clear its content
    # 	for the_file in os.listdir(outdir):
    # 		file_path = os.path.join(outdir, the_file)
    # 		if os.path.isfile(file_path):
    # 			os.remove(file_path)

	with open(intersections_filename,'r') as f:
    	intersections = [i.strip() for i in f.readlines()]
    print 'Intersections:', intersections
    intersection_locations = get_intersection_locations(intersections, geolocator)
    print intersection_locations

    route_log = pandas.read_table(log_filename)

    for i in xrange(len(intersections)):

        intersection_loc = intersection_locations[i]
        print "\nIntersection %d is at (%f, %f)" % \
            (i+1, intersection_loc.latitude, intersection_loc.longitude)

        # apply vincenty to each row, getting distance in meters
        distances = route_log.apply(lambda row: vincenty((row.Latitude, row.Longitude),
            (intersection_loc.latitude, intersection_loc.longitude)).meters, 1)

        mind = distances.min()
        if mind > THRESHOLD:
            print 'Minimal distance to specified intersection is %d (> %d) meters' % \
                (mind, THRESHOLD)
        else:
            print "Minimum distance", mind
            intersection_indices.append(distances.idxmin())

    print '==> Found', len(intersection_indices), 'intersections at indices:', intersection_indices
    if len(intersections) > len(intersection_indices):
        print bcolors.WARNING, 'Couln\'t find some intersections', bcolors.ENDC
    else:
        print bcolors.OKBLUE, 'Found all intersections', bcolors.ENDC

    # sort indices and append end of dataframe index
    intersection_indices.sort()
    intersection_indices.append(len(route_log))

    prev_idx = 0
    for i in xrange(len(intersection_indices)):
        idx = intersection_indices[i]
        segment_data = route_log[prev_idx:idx]
        prev_idx = idx
        prefix = os.path.splitext(os.path.basename(log_filename))[0]
        filename = os.path.join(outdir, '%s_segment_%d.csv' % (prefix, i + 1))
        segment_data.to_csv(filename, index=False, sep='\t')

if __name__ == '__main__':
    main()
