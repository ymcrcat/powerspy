#!/usr/bin/env python
import sys
import matplotlib
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import pandas
from geopy.distance import vincenty

def plot_hist(data, output_file):
    sns.set(style="white", palette="muted")
    b, g, r, p = sns.color_palette("muted", 4)
    sns.distplot(data, kde=False, color="dodgerblue")
    sns.despine()
    plt.ylabel('Error Count')
    plt.xlabel('Meters')

    plt.tight_layout()
    # plt.show()
    # plt.savefig(output_file + '.pgf')
    plt.savefig(output_file + '.eps')

    sns.kdeplot(data, cumulative=True)
    plt.savefig(output_file + '.cdf.eps')

def main():
    filename = sys.argv[1]
    output_file = sys.argv[2]
    # matplotlib.use("pgf")

    print 'Plotting data from', filename

    data = pandas.read_csv(filename, sep=',', header=None,
        names=['Time', 'TrueLon', 'TrueLat', 'EstLon', 'EstLat'])

    data['Error'] = data.apply(lambda row: vincenty((row.TrueLat, row.TrueLon),
        (row.EstLat, row.EstLon)).meters, 1)

    plot_hist(data['Error'], output_file)

if __name__ == '__main__':
    main()
