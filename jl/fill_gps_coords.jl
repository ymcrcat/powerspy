using ProgressMeter
using DataFrames

# Fill GPS coordinates in a log recorded without GPS coordinates, from
# another log that was recoding GPS coordinates
function fill_gps_coordinates(log_with_gps, log_with_no_gps)
    new_log = log_with_no_gps
    time_no_gps = log_with_no_gps[:Time]

    @showprogress 1 "Filling missing GPS coordinates..." for i in 1:length(time_no_gps)
				val, ind = findmin(abs(log_with_gps[:Time] - time_no_gps[i]))
        new_log[:Latitude][i] = log_with_gps[:Latitude][ind]
        new_log[:Longitude][i] = log_with_gps[:Longitude][ind]
    end

    return new_log
end

function fill_gps_coordinates_in_files(file_with_gps, file_no_gps, new_logfile)
    log_with_gps = readtable(file_with_gps, separator='\t')
    log_with_no_gps = readtable(file_no_gps, separator='\t')
    new_log = fill_gps_coordinates(log_with_gps, log_with_no_gps)
    writetable(new_logfile, new_log, separator='\t')
end

function main()
    fill_gps_coordinates_in_files(ARGS[1], ARGS[2], ARGS[3])
end

if !isinteractive()
    main()
end
