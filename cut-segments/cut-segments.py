#!/usr/bin/env python
from geopy.geocoders import GoogleV3
from geopy.distance import vincenty
from pprint import pprint
import os
import shutil
import ssl
import sys
from collections import namedtuple

Location = namedtuple("Location", "latitude longitude")

threshold = 100 # in meters
maxdistance = 100000 # in meters

def find_location(geolocator, intersection):
        while True:
                try:
			iloc = geolocator.geocode(intersection, timeout=5)
		except ssl.SSLError:
			print "Got an error connecting to Google Maps. Try again."
			continue
		break
	return iloc

def get_location_from_tuple(intersection):
        lat_str, lon_str = intersection.split(",")
        return Location(float(lat_str), float(lon_str))

def main():
    if len(sys.argv) < 3:
        sys.exit("Usage: cut_segements.py <route file> <intersection file> <output dir>")

    routefile = sys.argv[1]
    intersectionsfile = sys.argv[2]
    outdir = sys.argv[3]

    geolocator = GoogleV3()

    intersectionsfound  = {} # a dictionary the maps a point on the route (indexed by it line if the routefile) to an intersection

    if not os.path.exists(outdir):
        os.makedirs(outdir)
    else: # if directory already exists clear its content
    	for the_file in os.listdir(outdir):
    		file_path = os.path.join(outdir, the_file)
    		if os.path.isfile(file_path):
    			os.remove(file_path)

    isf = open(intersectionsfile,'r');
    rtf = open(routefile,'r');

    inum = 0
    iloc_list = []

    for i in isf:
    	rnum = 0
    	prevd = maxdistance
    	mind  = maxdistance
    	foundit = False
			
        # iloc = find_location(geolocator, i)
        iloc = get_location_from_tuple(i)
    	iloc_list.append(iloc)
    	print "\nIntersection " + i + " is at (" + str(iloc.latitude) + ", " + str(iloc.longitude) + ")"
    	rtf.seek(0)
    	for r in rtf:
    		arr = r.split("\t")
    		try:
    			rloc = (float(arr[6]), float(arr[7]))
    			#print rloc
    		except ValueError:
    			continue
    		d = vincenty(rloc, (iloc.latitude, iloc.longitude)).meters
    		mind = min(d,mind)
    		#print str(rnum) + ":" + str(d)
    		if prevd < threshold:
    			if d > prevd and foundit == False:
    				intersectionsfound[rnum-1] = inum
    				print "Found this intersection at " + str(rnum-1) + " at distance " + str(prevd) + "."
    				foundit = True
    		else:
    			foundit = False
    		prevd = d
    		rnum = rnum + 1
    	print "Minimum distance " + str(mind) + "."
    	inum = inum + 1	

    pprint(intersectionsfound)

    rtf.seek(0)
    current = -1;
    prev = -1
    prevprev = -1
    rnum = 0
    tempsegmentfile = outdir + "/temp"
    sf = None
    header = ""
    distance = 0 # the distance of the current measurements from the last intersection
    for r in rtf:
    	values = r.split("\t")
    	try:
    		x = float(values[0]) #just to check if this is a legal line (not a header or a comment)
    	except ValueError:
    		if rnum == 0: #this is the header line
    			header = r.rstrip() + "\tPower\tDistance\n" 
    		continue	
    	if sf is not None:
    		power = float(values[2]) * float(values[3]) #Volt * Current
    		if current != -1:
    			curr_location = (values[6], values[7])
    			distance = distance + vincenty(curr_location, last_location).meters
    			last_location = curr_location
    		sf.write(r.rstrip() + "\t" + str(power) + "\t" + str(distance) + "\n")
    	#print str(rnum)
    	if rnum in intersectionsfound:
    		prevprev = prev
    		prev = current
    		current = intersectionsfound[rnum]
    		distance = 0
    		last_location = (iloc_list[current].latitude, iloc_list[current].longitude)
    		#print "\nFound intersection at " + str(current) + "."
    		if prev != -1:
    			sf.close()
    			if prev != current:
    				segmentname = os.path.join(outdir, str(prevprev) + "_" + str(prev) + "_" + str(current))
    				index = 1
    				targetfilename = segmentname + "-" + str(index) + ".csv"
    				while os.path.exists(targetfilename):
    					index = index + 1
    					targetfilename = segmentname + "-" + str(index) + ".csv"
    				os.rename(tempsegmentfile, targetfilename)
    		sf = open(tempsegmentfile, 'w')
    		sf.write(header)
    	rnum = rnum + 1

    sf.close()
    os.remove(tempsegmentfile)
    rtf.close()
    isf.close()

if __name__ == '__main__':
    main()
