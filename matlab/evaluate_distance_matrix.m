function [mcr, train_unique_routes, test_routes_num] = ...
    evaluate_distance_matrix(log_list, distance_matrix, num_per_class)
    
    [train_set, test_set] = get_train_test_sets(log_list, num_per_class);
    fprintf('Train set size: %d, test set size: %d\n', length(train_set), length(test_set));
    
    train_unique_routes = length(unique(log_list.Label(train_set)));
    test_routes_num = length(test_set);
    
    fprintf('%d unique routes in training set\n', train_unique_routes);
    
    class = zeros(length(test_set), 1);
    train_labels = log_list.Label(ismember(log_list.ID, train_set));
    test_labels = log_list.Label(ismember(log_list.ID, test_set));
    
    % distance matrix sliced according to training set
    filtered_dm = distance_matrix(:, ismember(log_list.ID, train_set));
    
    for i = 1:length(test_set)
        idx = test_set(i);
        [~, class_ind] = min(filtered_dm(idx, :));
        class(i) = train_labels(class_ind);
        %fprintf('%s (labeled %d) classified as %d among %d references\n', ...
        %        log_list.Route{idx}, log_list.Label(idx), class(i), length(train_set));
        
        if class(i) ~= test_labels(i)
           fprintf('%s: %d misclassified as %d\n', ...
               log_list.Filename{test_set(i)}, test_labels(i), class(i));
        end
    end
    
% 
%     if plot_results
%         figure;
%         plotconfusion(targets, outputs);
%     end

    correct = class == test_labels;
    correct_rate = sum(correct) / length(correct);
    mcr = 1 - correct_rate;
    display(mcr);
end

function [train_set, test_set] = get_train_test_sets(log_list, num_per_class)
    train_set = [];
    labels = unique(log_list.Label);
    for l = labels'
       idx = log_list.ID(log_list.Label == l);
       if length(idx) < num_per_class + 1
           fprintf('Skipping %d. Not enough samples\n', l);
           continue
       end
       assert(length(idx) > 1);
       chosen_idx = randsample(idx, num_per_class);
       train_set = [train_set; chosen_idx];
    end
    
    % pick samples that have corresponding references in the chosen
    % training set, otherwise classification will trivially fail
    labels_in_training = unique(log_list.Label(ismember(log_list.ID, train_set)));
    has_reference_profiles = log_list.ID(ismember(log_list.Label, labels_in_training));
    test_set = setdiff(has_reference_profiles, train_set);
end