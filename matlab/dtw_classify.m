function class = dtw_classify(i, distance_matrix, train_ind, train_labels)
    % classification using minimal distance
    [~, ind] = min(distance_matrix(i, train_ind));
    assert(train_ind(ind) ~= i);
    t = whos('train_labels');
    if strcmp(t.class, 'cell')
        class = train_labels{ind};
    else
        class = train_labels(ind);
    end
end