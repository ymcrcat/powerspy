function d = gps_to_distance(p1, p2)
    % Convert GPS coordinates to distance in kilometers
    arclen = distance(p1.lat, p1.lon, p2.lat, p2.lon);
    d = deg2km(arclen);
end