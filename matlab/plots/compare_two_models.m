function compare_two_models
    addpath('..');
    LOG_DIR = '../../../BatLevelLogs';
    NEXUS_5 = '042335963089f20b';
%     NEXUS_4_1 = '00a697fa469633a5';
    NEXUS_4_2 = '0094e779d7d1841f';
%     NEXUS_4_3 = '04dc22d4dad7e4ce';
    
    SMOOTHING_WINDOW = 400;
    DOWNSAMPLE_FACTOR = 1;

    nexus4_2_data = read_data([LOG_DIR '/' NEXUS_4_2 '/tofalafel-20141101_173343.csv']);
    % bad recording
    %     nexus4_3_data = read_data([LOG_DIR '/' NEXUS_4_3 '/tofalafel-20141101_173756.csv']);
    nexus5_data = read_data([LOG_DIR '/' NEXUS_5 '/tofalafel-20141101_173448.csv']);
    
    processed_nexus4_power = preprocess_data(nexus4_2_data.Power, ...
        'SmoothingWindow', SMOOTHING_WINDOW, ...
        'DownsampleFactor', DOWNSAMPLE_FACTOR);
    processed_nexus5_power = preprocess_data(nexus5_data.Power, ...
        'SmoothingWindow', SMOOTHING_WINDOW, ...
        'DownsampleFactor', DOWNSAMPLE_FACTOR);
    
    nexus4_range = 12000:(length(processed_nexus4_power)-500);
    nexus5_range = 5000:(length(processed_nexus5_power)-500);
    
    start_time = nexus5_data.Time(1) / 1000;
    
    figure;
    plot(nexus4_2_data.Time(nexus4_range)/1000 - start_time, processed_nexus4_power(nexus4_range));
    hold all;
    plot(nexus5_data.Time(nexus5_range)/1000 - start_time, processed_nexus5_power(nexus5_range), 'r');
    
%     figure;
%     plot(aligned_data{1}(range), 'rx');
%     hold all;
%     plot(aligned_data{2}(range), 'b--');
    axis tight;
    legend('Nexus 4', 'Nexus5');
    xlabel('Time [sec]', 'FontSize', 15);
    ylabel('Normalized power', 'FontSize', 15);
    set(gca, 'box', 'off');
end