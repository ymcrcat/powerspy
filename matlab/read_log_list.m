function [log_list, routes] = read_log_list(log_filename)
    LOG_DIR = fileparts(log_filename);
    log_list = dataset('File', log_filename);
    
    routes = unique(log_list.Route);
    display('Unique routes:');
    display(routes);
    
    rows_num = length(log_list);
    log_data = cell(rows_num, 1);
    progressbar;
    for i = 1:rows_num
        filename = [LOG_DIR '/' log_list.Device{i} '/' log_list.Filename{i}];
        % fprintf(['Reading ' filename ' ...']);
        log_data{i} = read_data(filename);
        % fprintf(' done.\n');
        progressbar(i/rows_num);
    end
    
    log_list.Data = log_data;
    log_list.ID = (1:rows_num)'; % add an ID to each row
end