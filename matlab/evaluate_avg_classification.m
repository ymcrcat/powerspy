function results = evaluate_avg_classification(loglist_file)
    
    USE_CACHE = true;
    
    if ~USE_CACHE
        log_list = read_log_list(loglist_file);
        log_list_filter = ~strcmp(log_list.DeviceType, 'GalaxyS3');
        log_list = log_list(log_list_filter, :);
        
        [distance_matrix, ~] = preprocess_and_compute_distance(log_list);
        save distance_matrix.mat
    else
        load distance_matrix.mat
    end
    
    fprintf('%d entries in %s\n', length(log_list), loglist_file);
    fprintf('%d unique routes\n', length(unique(log_list.Label)));
    
    NUM_ITER = 20;
    NUM_PER_CLASS = 15; % minimal number of train samples per class
    
    results = zeros(NUM_ITER, 1);
    unique_routes = zeros(NUM_ITER, 1);
    test_routes = zeros(NUM_ITER, 1);
    for i = 1:NUM_ITER
        [results(i), unique_routes(i), test_routes(i)] = ...
            evaluate_distance_matrix(log_list, distance_matrix, NUM_PER_CLASS);
    end
    
    display(mean(results));
    display(mean(unique_routes));
    display(mean(test_routes));
end