function mcr = evaluate_dtw_classification(log_list)
    
    data_field_name = 'Power';
    
    SMOOTHING_WINDOW = 200;
    DS_FACTOR = 10; % Downsampling factor
   
    data_time = zeros(length(log_list), 1);
    
    % preprocess all data in log table
    preprocessed_data = cell(length(log_list), 1);
    for i = 1:length(log_list)
        data = log_list.Data{i};
        data_time(i) = data.RelativeTime(end);
        
        fprintf('Preprocessing %s\n', log_list.Filename{i});
        
        preprocessed_data{i} = ...
           preprocess_data(data.(data_field_name), ...
                'SmoothingWindow', SMOOTHING_WINDOW, ...
                'DownsampleFactor', DS_FACTOR);
    end
    
    % calculate DTW distance between every pair of sequences
    distance_matrix = compute_dtw_distance_matrix(preprocessed_data);
    
    class = cell(length(log_list), 1);
    
    for i = 1:length(log_list)
        [~, train_labels, ind_filter] = get_train_set(log_list, i);
        assert(any(ind_filter));
        train_ind = 1:length(log_list);
        train_ind(~ind_filter) = [];
        class{i} = dtw_classify(i, distance_matrix, train_ind, train_labels);
        fprintf('%s classified as %s among %d references\n', ...
                log_list.Route{i}, class{i}, length(train_labels));
    end
    
%     display(class);
%     display(log_list.Route);
    
    % prepare targets and outputs matrices for performance evaluation
    routes = unique(log_list.Route);
    num_routes = length(routes);
    targets = zeros(num_routes, length(class));
    outputs = zeros(num_routes, length(class));
    for i = 1:num_routes
       targets(i, strcmp(log_list.Route, routes{i})) = 1;
       outputs(i, strcmp(class, routes{i})) = 1;
    end
    
    fprintf('Misclassifications:\n');
    for i = 1:length(class)
        if ~strcmp(class{i}, log_list.Route{i})
           fprintf('%s: %s misclassified as %s\n', ...
               log_list.Filename{i}, log_list.Route{i}, class{i});
        end
    end
        
    plotroc(targets, outputs);
    figure;
    plotconfusion(targets, outputs);
    
    correct = strcmp(class, log_list.Route);
    correct_rate = sum(correct) / length(correct);
    mcr = 1 - correct_rate;
    display(mcr);
end

function [train_ids, train_labels, log_list_filter] = get_train_set(log_list, i)
    % Uncomment last condition to have reference profiles and test profiles
    % from different devices
    log_list_filter = log_list.ID ~= log_list.ID(i); % & ~strcmp(log_list.Device, log_list.Device{i});
    train_ids = log_list.ID(log_list_filter);
    train_labels = log_list.Route(log_list_filter);
end