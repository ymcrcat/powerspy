function d = get_dtw_distance(d1, d2, varargin)
%   Method useful for same length sequences:
%     SM = simmx(abs(d1), abs(d2));
%     [~, ~, D] = dp(1-SM);
%     d = D(size(D,1), size(D,2));

%   For different length sequences:
%     aligned_series = align_data({d1, d2}, false, varargin);
%     N = length(aligned_series{1});
%     d = norm(aligned_series{1} - aligned_series{2}) / N;
    
    if nargin > 2
        w = varargin{1}; % window size
        d = dtw_c(d1, d2, w);
    else
        d = dtw_c(d1, d2);
    end
    
    % we need to normalize the warping distance
    % we choose to normalize by dividing by the shorter sequence
    d = d / min(length(d1), length(d2));
end