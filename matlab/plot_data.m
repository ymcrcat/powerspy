function plot_data(varargin)
    POWER_SMOOTHING_WINDOW = 100;
    SIGNAL_SMOOTHING_WINDOW = 10;
    
    power_plot = figure;
    signal_plot = figure;
    sod_plot = figure;
    
    for i = 1:nargin
       data = varargin{i};
       smoothed_power = smooth(data.Power, POWER_SMOOTHING_WINDOW);
       smoothed_power = remove_dc_offset(smoothed_power);
       
       smoothed_signal_strength = smooth(data.Signal, SIGNAL_SMOOTHING_WINDOW);
       smoothed_signal_strength = remove_dc_offset(smoothed_signal_strength);
       
       sod = data.SOD;
       
       relative_time = data.RelativeTime;
       
       figure(power_plot);
       plot(relative_time, smoothed_power);
       hold all;
       
       figure(signal_plot);
       plot(relative_time, smoothed_signal_strength);
       hold all;
       
       figure(sod_plot);
       plot(relative_time, sod);
       hold all;
    end
    
    figure(power_plot);
    xlabel('Time');
    ylabel('Power')
    
    figure(signal_plot);
    xlabel('Time');
    ylabel('Signal strength');
    
    figure(sod_plot);
    xlabel('Time');
    ylabel('SOD');
end