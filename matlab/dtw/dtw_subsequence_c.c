/**
 * Copyright (C) 2014 Yan Michalevsky <yanm2@cs.stanford.edu>,
 * Department of Electrical Engineering,
 * Stanford University, Stanford, CA 94305, USA.
 * Based on previous source code by Quan Wang <wangq10@rpi.edu>,
 * Signal Analysis and Machine Perception Laboratory,
 * Department of Electrical, Computer, and Systems Engineering,
 * Rensselaer Polytechnic Institute, Troy, NY 12180, USA
 */

/** 
 * This is the C/MEX code of dynamic time warping of two signals
 *
 * compile: 
 *     mex dtw_subsequence_c.c
 *
 * usage:
 *     [d, a, b] = dtw_subsequence_c(s,t) or  
 *       [d, a, b] = dtw_subsequence_c(s,t,w)
 *     where s is signal 1, t is signal 2, w is window parameter 
 */

#include "mex.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <float.h>

/* #define DEBUG */

#ifdef DEBUG
#define TRACE(x) x
#else
#define TRACE(x)
#endif

const int INT_INF = -1;
const double DBL_INF = __DBL_MAX__;

int min(int a, int b) {
    return a < b ? a : b;
}

int max(int a, int b) {
    return a > b ? a: b;
}

unsigned int find_min_element(double* vec, unsigned int size) {
    unsigned int min_ind = -1;
    double min_val = __DBL_MAX__;
		unsigned int i;

    for (i = 0; i < size; ++i) {
        if (vec[i] <= min_val) {
            min_val = vec[i];
            min_ind = i;
        }
    }

    return min_ind;
}

double vectorDistance(double *s, double *t, unsigned int ns, unsigned int nt, unsigned int k, unsigned int i, unsigned int j)
{
    double result = 0;
    double ss,tt;
		unsigned int x;

    for(x = 0; x < k; x++) {
        ss = s[i+ns*x];
        tt = t[j+nt*x];
        result += ((ss-tt)*(ss-tt));
    }

    result = sqrt(result);
    return result;
} 

unsigned int getStartOffset(double** D, unsigned int n, unsigned int m)
{
	unsigned int a_ind = 0;

	while (n != 0 || m != 0) {
		if (n == 0) {
			--m;
			++a_ind;
		}
		else if (m == 0) {
			--n;
		}
		else {
			if ( (D[n-1][m] <= D[n-1][m-1]) && (D[n-1][m] <= D[n][m-1]) ) {
				--n;
			}
			else if ( (D[n][m-1] <= D[n-1][m-1]) && (D[n][m-1] <= D[n-1][m]) ) {
				--m;
			}
			else {
				--n;
				--m;
			}
		}

		TRACE( printf("n = %d, m = %d\n", n, m) );
	}

	return a_ind;
} 

unsigned int getEndOffset(double** D, unsigned int ns, unsigned int nt)
{
    return find_min_element(D[ns], nt); 
} 

double dtw_subsequence_c(double *s, double *t, int w, int ns, int nt, 
						 int k, int* a, int* b)
{
    double d = 0;
    int sizediff = (ns-nt) > 0 ? (ns-nt) : (nt-ns);
    double** D = NULL;
    unsigned int i, j;
    unsigned int j1, j2;
    double cost, temp;

    /* printf("ns=%d, nt=%d, w=%d, s[0]=%f, t[0]=%f\n",ns,nt,w,s[0],t[0]); */
    
    if (w != INT_INF && w < sizediff) { 
        w = sizediff; /* adapt window size */
    }
    
    /* create D */
    D = (double**) malloc((ns+1)*sizeof(double *));
    for(i = 0; i < ns + 1; i++) {
        D[i]=(double *) malloc((nt+1)*sizeof(double));
    }
    
    /* initialization to Inf */
    for(i = 0;i < ns + 1; i++) {
        for(j=0;j<nt+1;j++) {
            D[i][j] = DBL_INF;
        }
    }
    D[0][nt - 1] = 0;

    /* subsequence initialization */
    D[0][0] = vectorDistance(s, t, ns, nt, k, 0, 0);
    for (i = 1; i < ns; ++i) {
      D[i][0] = vectorDistance(s, t, ns, nt, k, i, 0) + D[i - 1][0];
    }

    for (i = 0; i < nt; ++i) {
      D[0][i] = vectorDistance(s, t, ns, nt, k, 0, i);
    }
    
    /* dynamic programming */
    for(i = 1; i <= ns; i++) {
        if(w == INT_INF) {
            j1=1;
            j2=nt;
        }
                else {
            j1 = max(i - w, 1);
            j2 = min(i + w, nt);
        }

        for(j=j1;j<=j2;j++) {
            cost=vectorDistance(s,t,ns,nt,k,i-1,j-1);
            
            temp=D[i-1][j];
            if(D[i][j-1] != DBL_INF) 
            {
                if(temp == DBL_INF || D[i][j-1]<temp) temp=D[i][j-1];
            }
            if(D[i-1][j-1] != DBL_INF) 
            {
                if(temp == DBL_INF || D[i-1][j-1]<temp) temp=D[i-1][j-1];
            }
            
            D[i][j]=cost + temp;
        }
    }
     
#ifdef DEBUG
    /* view matrix D */
    for(i=0;i<ns+1;i++)
    {
        for(j=0;j<nt+1;j++)
        {
            printf("%f  ",D[i][j]);
        }
        printf("\n");
    }
#endif

    *b = getEndOffset(D, ns, nt);
    *a = getStartOffset(D, ns, *b);
    
    d = D[ns][*b];

    for(i=0;i<ns+1;i++)
    {
        free(D[i]);
    }
    free(D);
    
    ++*a;
		++*b;

    return d;
} 

/* the gateway function */
void mexFunction( int nlhs, mxArray *plhs[],
                  int nrhs, const mxArray *prhs[])
{
    double *s = NULL, *t = NULL;
    int w;
    int ns, nt, k;
    double *dp = NULL;
    unsigned int* a = NULL; /* start offset of subsequence */
    unsigned int* b = NULL; /* end offset of subsequence */

    /*  check for proper number of arguments */
    if (nrhs != 2 && nrhs != 3) {
        mexErrMsgIdAndTxt( "MATLAB:dtw_subsequence_c:invalidNumInputs",
                "Two or three inputs are required.");
    }

    if (nlhs < 1 || nlhs > 3) {
        mexErrMsgIdAndTxt( "MATLAB:dtw_subsequence_c:invalidNumOutputs",
                "dtw_subsequence_c: One to three outputs are required.");
    }
    
    /* check to make sure w is a scalar */
    if(nrhs == 2) {
        w=-1;
    } else if (nrhs==3) {
        if( !mxIsDouble(prhs[2]) || mxIsComplex(prhs[2]) ||
                mxGetN(prhs[2])*mxGetM(prhs[2])!=1 )
        {
            mexErrMsgIdAndTxt( "MATLAB:dtw_c:wNotScalar",
                    "dtw_c: Input w must be a scalar.");
        }
        
        /*  get the scalar input w */
        w = (int) mxGetScalar(prhs[2]);
    } /* if window argument specified */
    
    /*  create a pointer to the input matrix s */
    s = mxGetPr(prhs[0]);
    
    /*  create a pointer to the input matrix t */
    t = mxGetPr(prhs[1]);
    
    /*  get the dimensions of the matrix input s */
    ns = mxGetM(prhs[0]);
    k = mxGetN(prhs[0]);
    
    /*  get the dimensions of the matrix input t */
    nt = mxGetM(prhs[1]);
    if(mxGetN(prhs[1])!=k) {
        mexErrMsgIdAndTxt( "MATLAB:dtw_c:dimNotMatch",
                "dtw_c: Dimensions of input s and t must match.");
    }  
    
    /*  set the output pointer to the output matrix */
    plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
    plhs[1] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
    plhs[2] = mxCreateNumericMatrix(1, 1, mxUINT32_CLASS, mxREAL);
    
    /*  create a C pointer to a copy of the output matrix */
    dp = mxGetPr(plhs[0]);
    a = (unsigned int*) mxGetData(plhs[1]);
    b = (unsigned int*) mxGetData(plhs[2]);
    
    /*  call the C subroutine */
    dp[0] = dtw_subsequence_c(s,t,w,ns,nt, k, a, b);
    
    return;
} /* end of mexFunction */
