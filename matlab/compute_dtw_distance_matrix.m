function distance_matrix = compute_dtw_distance_matrix(data)
%     MAX_TIME_DIFF = 30 * 60 * 1000; % 30 minutes
    n = length(data);
    distance_matrix = inf(n);
    progressbar;
    for i = 1:n
       for j = i+1:n         
           warp_window = floor(min(length(data{i}), length(data{j})) / 10);
           distance_matrix(i, j) = ...
              get_dtw_distance(data{i}, data{j}, warp_window);
       end
       progressbar(i/n);
    end
    
    distance_matrix = triu(distance_matrix) + triu(distance_matrix)';
end