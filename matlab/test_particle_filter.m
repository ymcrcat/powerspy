function test_particle_filter
    % Test ParticleFilter implementation
    position = 0:0.1:6;
    terrain = sin(position);
    % terrain = position;
%     plot(position, terrain);
    
    y = 2;
    reference_dist = y - terrain;
    
    NUM_PARTICLES = 100;
    particle_filter = ParticleFilter({reference_dist'}, NUM_PARTICLES);
    
    h = figure;
    for i=1:length(position)
        x = position(i);
        meas = y - terrain(i);
        particle_filter.update(meas, 1);
        
        figure(h);
        plot(position, terrain);
        hold all;
        scatter(x, y, 100, 'fill');
        chosen = find(particle_filter.weights >= ...
            prctile(particle_filter.weights, 95));
        scatter(position(particle_filter.offset(chosen)), ...
            ones(length(particle_filter.offset(chosen)), 1) * 1.5, ...
            100, 'fill');
        hold off;
    end
end