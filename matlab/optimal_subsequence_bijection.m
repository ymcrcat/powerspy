function [d, a, b] = optimal_subsequence_bijection(target, query, warp_window, querySkip, targetSkip, jumpCost)
        m = length(query);
        n = length(target);
        
        dist = Inf(m+2, n+2);
        for i=2:m+1
            for j=2:n+1
                dist(i,j) = distance(query(i-1,1), target(j-1,1));
            end
        end
        
        dist(1,1) = 0;
        dist(m+2,n+2) = 0;
        
        weight = Inf(m+2, n+2);
        weight(1,1:n+2) = dist(1,1:n+2);
        weight(1:m+2,1) = dist(1:m+2,1);
        
        for i=1:m+1
            % Find index of last query element that may be skipped
            stopRowJump = min(i + querySkip, m+2);
            
            for j=1:n+1
                % Find index of last target element that may be skipped
                stopColJump = min(j + targetSkip, n+2);
                
                if abs(i-j) <= warp_window
                    for rowJump = i+1:stopRowJump
                        for colJump = j+1:stopColJump
                            newWeight = weight(i,j) + dist(rowJump, colJump) + ...
                                ((rowJump - i - 1) + (colJump - j - 1))*jumpCost;
                            if newWeight < weight(rowJump, colJump)
                                weight(rowJump, colJump) = newWeight;
                            end
                        end
                    end
                end
            end            
        end
        
        [d, b] = min(weight(m + 1, 2:n));

        % find a through optimal warping path
        p = optimal_warping_path(weight, [m+2, b]);
        p = flip(p);
        %     display(p);
    
        a = find(p(:, 1) == 1, 1, 'last');
        
end

% OSB uses Euclidean distance. 
% Modify this function to use other distance metrics.
function dist = distance(a, b)
    dist = (a - b)^2;
end

% The same function to find start offset doesn't work with OSB
% as it works with DTW.
% TODO : Modify the optimal warping path function
function p = optimal_warping_path(D, p)
    % D - accumulated cost matrix
    % p - initial path
    
    n = p(end, 1);
    m = p(end, 2);
    while ~((n == 1) && (m == 1))
        if n == 1
            pl_prev = [1, m-1];
        elseif m == 1
            pl_prev = [n-1, 1];
        else
            sub_ind = [ [n-1, m-1]; [n, m-1]; [n-1, m] ];
            ind = sub2ind(size(D), sub_ind(:, 1), sub_ind(:, 2));
            [~, min_ind] = min(D(ind));
            [i, j] = ind2sub(size(D), ind(min_ind));
            pl_prev = [i j];
        end

        p = [p; pl_prev];
        n = p(end, 1);
        m = p(end, 2);
    end
end
